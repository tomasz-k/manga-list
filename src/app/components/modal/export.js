'use client'

import styles from '../../styles/modal.module.css';
import { useStorage } from '../storage';

const ModalExport = () => {
  const { storageData, modalFunctions } = useStorage();
  const getString = () => {
    return JSON.stringify(storageData);
  };
  const copy = () => {
    navigator.clipboard.writeText(getString());
  };
  return (
    <>
      <header className={styles.header}>
        <div className={styles.essential_info}>
          <h2>Data export</h2>
          <p>To save the data you must copy content of the textarea</p>
        </div>
      </header>
      <div className={styles.commands}>
        <textarea className={styles.textarea} placeholder="JSON output" defaultValue={getString()} disabled/>
        <div className={styles.modal_footer}>
          <button className={styles.button} onClick={ () => { 
            copy();
            modalFunctions.hideModal();
          }}>COPY</button>
        </div>
      </div>
    </>
  )
}

export default ModalExport;