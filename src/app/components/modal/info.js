
import styles from '../../styles/modal.module.css';

const ModalInfo = ( { data } ) => {
  return (
    <>
      <header className={styles.header}>
      <img className={styles.header_image} src={data.images.webp.image_url}></img>
      <div className={styles.essential_info}>
        <h2>{data.title_english ?? data.title ?? data.title_japanese}</h2>
        <h3 className={styles.essential_info_h3}>{ data.score } by { data.scored_by }</h3>
        <h3 className={styles.essential_info_h3}>Rank at MAL: { data.rank }</h3>
        <br/>
        <p className={styles.essential_info_p}>Published: { data.published.string }</p>
        <p className={styles.essential_info_p}><span>Authors: </span>
          { data.authors.map((result, index) => ( 
            <span key={index}>
              <a href={result.url}>{result.name}</a> 
              {index !== data.authors.length - 1 && ", "}
            </span>
          ))}
        </p>
        <p className={styles.essential_info_p}><span>Genres: </span>  
          { data.genres.map((result, index) => ( 
            <span key={index}>
              <a href={result.url}>{result.name}</a> 
              {index !== data.authors.length - 1 && ", "}
            </span>
          ))}
        </p>
      </div>
      </header>
      <div className={styles.info}>
        <p>{data.synopsis}</p>
      </div>
    </>
  )
}

export default ModalInfo;