'use client'

import styles from '../../styles/modal.module.css';

import { useStorage } from '../storage';
import { useDebouncedCallback } from 'use-debounce';

import ModalInfo from './info';

const ModalModify = ( { data, hideModal } ) => {
  const { storageFunctions } = useStorage();
  const waitForSaving = (value = true) => {
    let textArea = document.querySelector('textarea');
    if(textArea) textArea.classList.toggle(styles.wait, value);
  };
  const handleDescription = useDebouncedCallback((query) => {
    data.user_data.description = query;
    //modifyDescrpition(query);
    waitForSaving(false);
    storageFunctions.updateStorage();
  }, 500);
  const handleStatus = (query) => {
    data.user_data.status = query;
    //modifyStatus(query);
    storageFunctions.updateStorage();
  };
  const removeFromStorage = () => {
    storageFunctions.removePosition(data);
    hideModal();
  };
  return (
    <>
      <ModalInfo data={data.mal_data}/>
      <div className={styles.commands}>
        <textarea onChange={(event) => { 
            handleDescription(event.target.value);
            waitForSaving();
          }
        } className={styles.textarea} placeholder="Place for your description" defaultValue={data.user_data.description}/>
        <div className={styles.modal_footer}>
          <button className={styles.button} onClick={ () => { removeFromStorage() }}>REMOVE</button>
          <select className={styles.select} onChange={(event) => handleStatus(event.target.value)} name="status" id="status" defaultValue={data.user_data.status}> 
            <option value={0}>Reading</option>
            <option value={1}>Plan to read</option>
            <option value={2}>Finished</option>
            <option value={3}>Dropped</option>
          </select>
        </div>
      </div>
    </>
  );
}

export default ModalModify;