'use client'

import { useEffect } from 'react';
import styles from '../../styles/modal.module.css';

import { useStorage } from '../storage';

const Modal = () => {
  const { modalFunctions, modalData } = useStorage();
  useEffect(() => {
    let body = document.querySelector('main');
    if(body) {
      body.classList.toggle(styles.lock, modalData !== null);
    }
  }, [ modalData ]);
  return modalData && (
    <>
      <div onClick={() => modalFunctions.hideModal() } className={styles.background}></div>
      <div className={styles.modal}>
        <div className={styles.container}>
          <modalData.component data={modalData.object} hideModal={modalFunctions.hideModal} />
        </div>
        <div className={styles.spacer}></div>
      </div>
    </>
  )
}

export default Modal;