'use client'

import styles from '../../styles/modal.module.css';

import { useStorage } from '../storage';

import ModalInfo from './info';

const ModalAdd = ( { data, hideModal } ) => {
  const { storageFunctions } = useStorage();
  const addToStorage = () => {
    storageFunctions.addPosition(data);
    hideModal();
  };
  return (
    <>
      <ModalInfo data={data} />
      <div className={styles.commands}>
        <button className={styles.button} onClick={ () => { addToStorage() }}>Add to list</button>
        <select className={styles.select} name="status" id="status">
          <option value="0">Read</option>
          <option value="1">Plan to read</option>
          <option value="2">Finished</option>
          <option value="3">Dropped</option>
        </select>
      </div>
    </>
  )
}

export default ModalAdd;