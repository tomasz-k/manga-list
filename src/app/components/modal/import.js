'use client'

import styles from '../../styles/modal.module.css';
import { useStorage } from '../storage';

const ModalImport = () => {
  const { storageFunctions, modalFunctions } = useStorage();
  const insert = () => {
    let element = document.querySelector('#insert-area');
    if(element) {
      storageFunctions.overwriteStorage(element.value);
    }
  };
  return (
    <>
      <header className={styles.header}>
        <div className={styles.essential_info}>
          <h2>Data import</h2>
          <p>Copy your saved content, push it into text area and click insert to restore your data</p>
        </div>
      </header>
      <div className={styles.commands}>
        <textarea id="insert-area" className={styles.textarea} placeholder="Paste your data..." />
        <div className={styles.modal_footer}>
          <button className={styles.button} onClick={ () => { 
            insert(); 
            modalFunctions.hideModal();
          }}>INSERT</button>
        </div>
      </div>
    </>
  )
}

export default ModalImport;