'use client'

import styles from '../styles/navigation.module.css';

import { useState } from 'react';
import { useDebouncedCallback } from 'use-debounce';

import axios from 'axios';

import ModalAdd from './modal/add';

import { useStorage } from './storage';

const Navigation = () => {
  // hooks
  const { modalFunctions } = useStorage();
  const [typing, setTyping] = useState(false);
  const [results, setResults] = useState([]);
  // functions
  const handleSelect = (result) => {
    modalFunctions.showModal(ModalAdd, result);
    setResults([]);
  };
  const handleSearch = useDebouncedCallback((query) => {
    if(query === '') {
      setResults([]);
      return;
    } 
    axios
    .get(`https://api.jikan.moe/v4/manga?q=${query}&limit=5`, {
      retry: 2,
      retryDelay: 200
    })
    .then((response) => {
      setResults(response.data.data);
    })
    .catch((error) => {
      console.log(error);
    });
  }, 300);
  const startSearch = (query) => {
    if(query !== '') {
      handleSearch(query);
    }
    setTyping(true);
  };
  const finishSearch = useDebouncedCallback(() => {
    setTyping(false);
    setResults([]);    
  }, 200);
  return (
    <>
      <nav className={styles.nav}>
        <div className={styles.container}>
          <div className={styles.search}>
            <span className={styles.logo}>M</span>
            <input className={styles.input} placeholder='Search new title...'
              onFocus={(event) => startSearch(event.target.value)} 
              onBlur={() => finishSearch()} 
              onChange={(event) => handleSearch(event.target.value)} 
              type='text'>
            </input>
          </div>
        </div>
      </nav>
      {(results && typing) && 
      <>
        <div className={styles.search_backdrop}></div>
        <div div className={styles.container}>
          <ul className={styles.result}>
            {results.length !== 0 ?
              (results.map((result) => (
                <li onClick={(event) => { handleSelect(result) }} className={styles.result_item} key={result.mal_id}>
                  <img className={styles.result_img} src={result.images.webp.small_image_url} />
                  <div className={styles.result_desc}>
                    <h2 className={styles.result_h2}>{result.title_english ?? result.title ?? result.title_japanese}</h2>
                    <p className={styles.result_p}>{result.type + ' (' + result.status + ')'}</p>
                    <p className={styles.result_p}>{result.synopsis && result.synopsis.slice(0, 100)}(...)</p>
                  </div>
                </li>
              ))) : (
                <li className={styles.result_item}>
                  <span className={styles.result_load}></span>
                  <div className={styles.result_desc}>
                    <h2 className={styles.result_h2}>We are trying to find your manga...</h2>
                    <p className={styles.result_p}>But you are sure it even exists...?</p>
                  </div>
                </li>
              )}
          </ul>
        </div>
      </>}
    </>
  )
}

export default Navigation;