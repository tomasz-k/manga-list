'use client'

import { createContext, useContext, useState, useLayoutEffect, Component } from 'react';

export const StorageContext = createContext([], () => {});
export const useStorage = () => useContext(StorageContext);

const StorageProvider = ({ children }) => {
  // hooks
  const [ storageData, setData ] = useState(null);
  const [ modalData, setModalData ] = useState(null);
  // functions
  const load = () => {
    let object = localStorage.getItem('data');
    if(object) {
      setData(JSON.parse(object));
    } else {
      setData([]);
    }
  };
  const save = (object) => {
    localStorage.setItem('data', JSON.stringify(object)); 
  };
  const storageFunctions = {
    overwriteStorage: (data) => {
      if(data.length === 0) 
        return;
      
        try {
        let newData = JSON.parse(data);
        setData(newData);
        save(newData);
      } catch(e) {
        console.log(e);
      }
    },
    updateStorage: () => {
      let newData = [ ...storageData ];
      setData(newData);
      save(newData);
    },
    addPosition: (object) => {
      if(storageFunctions.getKey(object)) {
        return false;
      }
      let newData = [ ...storageData, { mal_data: object, user_data: { description: '', status: '0' } } ];
      setData(newData);
      save(newData);
      return true;
    },
    removePosition: (object) => {
      if(!storageFunctions.getKey(object.mal_data)) {
        return false;
      }
      let newData = [ ...storageData ];
      const filteredObject = newData.filter((key) => {
          return key.mal_data.mal_id !== object.mal_data.mal_id
        });
      setData(filteredObject);
      save(filteredObject);
      return true;
    },
    getKey: (searchObject) => {
      let keyExists = null;
      for (const object of storageData) {
        if (object.mal_data.mal_id === searchObject.mal_id) {
          keyExists = object;
          break;
        }
      }
      return keyExists;
    }
  };
  const modalFunctions = {
    showModal: (component, object = null) => {
      let table = { 
        component: component,
        object: object
      };
      setModalData(table);
    },
    hideModal: () => {
      setModalData(null);
    }
  };
  useLayoutEffect(() => {
    load();
  }, []);
  return (
    <StorageContext.Provider value={{ storageFunctions, storageData, modalFunctions, modalData }}>
      {children}
    </StorageContext.Provider>
  );
}

export default StorageProvider;