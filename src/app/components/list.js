'use client'

import styles from '../styles/list.module.css';

import { useEffect, useState } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { useStorage } from './storage';

import ModalModify from './modal/modify';
import ModalExport from './modal/export';
import ModalImport from './modal/import';

const List = () => { 
  // hooks
  const { storageData, modalFunctions } = useStorage();
  const [filtredStorage, setFiltredStorage] = useState(null);
  const [sortOrder, setSortOrder] = useState('asc');
  // functions
  const sortByKey = (key) => {
    let sortedData = [...storageData];
    let isAsc = sortOrder === 'asc';
    sortedData.sort((a, b) => {
      let title_a = a.mal_data[key], title_b = b.mal_data[key];
      let compare = 0;
      if (title_a < title_b) {
        compare = -1;
      } else if (title_a > title_b) {
        compare = 1;
      }
      return isAsc ? compare : -compare;
    });
    setFiltredStorage(sortedData);
    setSortOrder(isAsc ? 'desc' : 'asc');
  };
  const getStatusTitle = (value) => {
    let returnValue = 'null';
    switch(value)
    {
      case '0': returnValue = 'Reading'; break;
      case '1': returnValue = 'Plan to read'; break;
      case '2': returnValue = 'Finished'; break;
      case '3': returnValue = 'Dropped'; break;
    }
    return returnValue;
  }
  const handleSearch = (query) => {
    let updateStorage =  [ ...storageData ];  
    setFiltredStorage(updateStorage.filter((item) => {
      return item.mal_data.title.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    }));
  };
  useEffect(() => {
    setFiltredStorage(storageData);
  }, [ storageData ]);
  // functions
  return (
    <>
      <div className={styles.container}>
        <div className={styles.search}>
          <div className={styles.hrefs}>
            <ol className={styles.list}>
              <li><button onClick={()=> { alert('not implemented!') }} className={ [ styles.list_item, styles['list_item--red']].join(' ') }>Share</button></li>
              <li><button onClick={()=> { modalFunctions.showModal(ModalExport) }} className={styles.list_item}>Export</button></li>
              <li><button onClick={()=> { modalFunctions.showModal(ModalImport) }} className={styles.list_item}>Import</button></li>
            </ol>
          </div>
          <div className={styles.form}>
            <input className={styles.input} placeholder='Search through list...'
              onChange={(event) => handleSearch(event.target.value)} 
              type='text'>
            </input>
          </div>
        </div>
        <table className={styles.table}>
          <thead>
            <tr className={styles.tr}>
              <th width={'2%'} className={[styles.th, styles.th_blank ].join(' ')}>#</th>
              <th width={'48%'} className={styles.th} onClick={ () => { sortByKey('title') }}>Title</th>
              <th width={'10%'} className={styles.th} onClick={ () => { sortByKey('score') }}>Score</th>
              <th width={'5%'} className={styles.th} onClick={ () => { sortByKey('type') }}>Type</th>
              <th width={'30%'} className={[styles.th, styles.th_progress, styles.th_blank ].join(' ')}>Description</th>
              <th width={'5%'} className={[styles.th, styles.th_blank].join(' ')}>Status</th>
            </tr>
          </thead>
          <TransitionGroup component="tbody">
            {filtredStorage !== null ? (filtredStorage.length !== 0 ?
              filtredStorage.map((object, index) => {
                let data = object.mal_data;
                let user = object.user_data;
                return (
                  <CSSTransition key={index} timeout={500} classNames="fade">
                  <tr className={styles.tr} onClick={()=> { modalFunctions.showModal(ModalModify, object) }} key={data.mal_id}>
                    <td className={styles.td}>{index}</td>
                    <td className={styles.td}>
                      <div className={styles.desc}>
                        <img className={styles.image} src={data.images.webp.small_image_url}></img>
                        <div className={styles.text_container}>
                          <p className={styles.text}>{data.title_english ?? data.title ?? data.title_japanese}</p>
                          <p className={styles.subtext}>{data.type} (Chapters: {data.chapters})</p>
                          <p className={styles.subtext}>{data.published.string}</p>
                        </div>
                      </div>
                    </td>
                    <td className={styles.td}>{data.score}</td>
                    <td className={styles.td}>{data.type}</td>
                    <td className={ [ styles.td, styles.td_progress ].join(' ')}>{(user.description && user.description.length) > 0 ? user.description : 'Place for your description'}</td>
                    <td className={styles.td}>{getStatusTitle(user.status)}</td>
                  </tr>
                  </CSSTransition>
                )
              }) : (
                <CSSTransition timeout={500} classNames="fade">
                <tr className={[ styles.tr, styles.th_blank ].join(' ')}>
                  <td colSpan={6} className={styles.td}>Your list is empty!</td>
                </tr> 
                </CSSTransition>
              )) : (
                <CSSTransition timeout={500} classNames="fade">
                <tr className={styles.tr}>
                  <td colSpan={6} className={styles.td}>Loading!</td>
                </tr>
                </CSSTransition>
              )}
          </TransitionGroup>
        </table>
      </div>
    </>
  )
}

export default List;