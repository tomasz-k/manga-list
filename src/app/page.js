'use client'

import StorageProvider from './components/storage';
import Navigation from './components/navigation';
import List from './components/list';
import Modal from './components/modal/modal';

export default function app() {
  return (
    <>
      <div className='backdrop'/>
      <StorageProvider>
      <Modal/>
      <Navigation/> 
      <main>
        <List/>
      </main>
      </StorageProvider>
    </>
  )
}
